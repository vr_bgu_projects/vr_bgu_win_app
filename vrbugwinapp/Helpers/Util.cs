﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace vrbugwinapp.Helpers
{
    public class Util
    {
        public static bool IsRecognisedImageFile(string fileName)
        {
            string targetExtension = System.IO.Path.GetExtension(fileName);
            if (String.IsNullOrEmpty(targetExtension))
                return false;
            else
                targetExtension = "*" + targetExtension.ToLowerInvariant();

            List<string> recognisedImageExtensions = new List<string>();

            foreach (System.Drawing.Imaging.ImageCodecInfo imageCodec in System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders())
                recognisedImageExtensions.AddRange(imageCodec.FilenameExtension.ToLowerInvariant().Split(";".ToCharArray()));

            foreach (string extension in recognisedImageExtensions)
            {
                if (extension.Equals(targetExtension))
                {
                    return true;
                }
            }
            return false;
        }

        public static string GetFullFilePath(string file)
        {
            string fullname = null;
            try
            {
                FileInfo f = new FileInfo(file);
                fullname = f.FullName;
            }
            catch (Exception ex)
            {
                throw;
            }

            return fullname;
        }

        public static bool CreateFolder(string tempFolderPath)
        {
            DirectoryInfo dir = System.IO.Directory.CreateDirectory(tempFolderPath);
            bool exists = System.IO.Directory.Exists(dir.FullName);
            return exists;
        }

        public static string GetFileExtension(string filePath)
        {
            return Path.GetExtension(filePath).Trim('.');
        }

        public static ProcessResult RunProcess(string processFileName, string arguments)
        {
            ProcessResult pr = new ProcessResult();
            StringBuilder result = new StringBuilder();
            try
            {
                var proc = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = processFileName,
                        Arguments = arguments,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true,
                        RedirectStandardError = true
                    }
                };
                // Create new stopwatch.
                Stopwatch stopwatch = new Stopwatch();

                // Begin timing.
                stopwatch.Start();
                proc.Start();
                stopwatch.Stop();

                while (!proc.StandardOutput.EndOfStream)
                {
                    string line = proc.StandardOutput.ReadLine();
                    result.AppendLine(line);
                    Logger.log.Debug(line);
                }
                pr.Result = result.ToString();
                pr.IsError = false;
                Logger.log.Debug(string.Format("Elapsed Time (ms): {0}", stopwatch.ElapsedMilliseconds));
                if (proc.ExitCode != 0)
                {
                    string errors = proc.StandardError.ReadToEnd();
                    if (!string.IsNullOrEmpty(errors))
                    {
                        pr.Result = errors;
                        pr.IsError = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return pr;
        }

        public static ProcessResult RunProcess2(string processFileName, string arguments)
        {
            ProcessResult pr = new ProcessResult();
            StringBuilder result = new StringBuilder();
            try
            {
                var proc = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = processFileName,
                        Arguments = arguments,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }
                };
                // Create new stopwatch.
                Stopwatch stopwatch = new Stopwatch();

                // Begin timing.
                stopwatch.Start();
                proc.Start();
                stopwatch.Stop();

                while (!proc.StandardOutput.EndOfStream)
                {
                    string line = proc.StandardOutput.ReadLine();
                    result.AppendLine(line);
                    Logger.log.Debug(line);
                }
                pr.Result = result.ToString();
                pr.IsError = false;
                Logger.log.Debug(string.Format("Elapsed Time (ms): {0}", stopwatch.ElapsedMilliseconds));
                //if (proc.ExitCode != 0)
                //{
                //    string errors = proc.StandardError.ReadToEnd();
                //    if (!string.IsNullOrEmpty(errors))
                //    {
                //        pr.Result = errors;
                //        pr.IsError = true;
                //    }
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return pr;
        }

        public static bool IsValidXml(string xmlString)
        {
            Regex tagsWithData = new Regex("<\\w+>[^<]+</\\w+>");

            //Light checking
            if (string.IsNullOrEmpty(xmlString) || tagsWithData.IsMatch(xmlString) == false)
            {
                return false;
            }

            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(xmlString);
                return true;
            }
            catch (Exception e1)
            {
                return false;
            }
        }

        public static string SaveFileToRunFolder(string text, string fullFilePath, string defaultFileExt)
        {
            string filename = fullFilePath + "." + defaultFileExt;
            try
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(filename, false))
                {
                    file.WriteLine(text);
                }
            }
            catch (Exception ex)
            {
                throw;
                return "An error occurred:\n" + ex.Message + "\nwhile trying to save file";
            }

            return String.Format("File {0} Saved successfully", fullFilePath);
        }

        public static bool SaveFile(string text, string fullFilePath)
        {
            try
            {
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(fullFilePath, false))
                {
                    file.WriteLine(text);
                }
            }
            catch (Exception ex)
            {
                throw;
                return false;
            }
            return true;
        }

        public static List<string> StringToList(string str, string comma)
        {
            List<string> list = new List<string>();
            string[] result = str.Split(new string[] { comma },StringSplitOptions.RemoveEmptyEntries).Select(elem => elem.Trim()).ToArray();
            list = result.ToList<string>();
            return list;
        }

        public static List<string> CommaSeperatedStringToList(string str)
        {
            return StringToList(str, ",");
        }

        public static string OpenFile(string fileName)
        {
            string text = "";
            try
            {
                text = (File.ReadAllText(fileName));
            }
            catch (Exception ex)
            {
                throw;
                return "An error occurred:\n" + ex.Message + "\nwhile trying to open file";
            }

            return text;
        }

        public static string ProperCase(string str)
        {
            return System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
        }

        public static bool IsOdd(int value)
        {
            return value % 2 != 0;
        }

        public static bool IsEven(int value)
        {
            return value % 2 == 0;
        }

        public static string ReplaceToUnixSlashes(string path)
        {
            return path.Replace(@"\", "/").Trim();
        }

        public static string GetExeApplicationPath()
        {
            return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        }

        public static object Deserialize<t>(String filename)
        {
            if (string.IsNullOrEmpty(filename))
            {
                return default(object);
            }
            try
            {
                XmlSerializer _xmlSerializer = new XmlSerializer(typeof(object));
                Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read);
                var result = (object)_xmlSerializer.Deserialize(stream);
                stream.Close();
                return result;
            }
            catch (Exception ex)
            {
                throw;
                return default(object);
            }
        }

        public static string[] ReplaceInStringArray(string[] items, string oldValue, string newValue)
        {
            for (int index = 0; index < items.Length; index++)
                items[index] = items[index].Replace(oldValue, newValue);

            return items;
        }

        //Implemented based on interface, not part of algorithm - not working good!!
        public static string RemoveAllNamespaces(string xmlDocument)
        {
            XElement xmlDocumentWithoutNs = stripNS(XElement.Parse(xmlDocument));

            return xmlDocumentWithoutNs.ToString();
        }

        public static XElement stripNS(XElement root)
        {
            XElement res = new XElement(
                root.Name.LocalName,
                root.HasElements ?
                    root.Elements().Select(el => stripNS(el)) :
                    (object)root.Value
            );

            res.ReplaceAttributes(
                root.Attributes().Where(attr => (!attr.IsNamespaceDeclaration)));

            return res;
        }

        public static XmlDocument RemoveXmlns(XmlDocument doc)
        {
            XDocument d;
            var xmlDocument = new XmlDocument();
            try
            {
                using (var nodeReader = new XmlNodeReader(doc))
                    d = XDocument.Load(nodeReader);

                d.Root.Descendants().Attributes().Where(x => x.IsNamespaceDeclaration).Remove();

                foreach (var elem in d.Descendants())
                    elem.Name = elem.Name.LocalName;

                using (var xmlReader = d.CreateReader())
                    xmlDocument.Load(xmlReader);


            }
            catch (Exception ex)
            {
                throw;
            }
            return xmlDocument;
        }

        public static string RemoveXmlns(String xml)
        {
            var xmlDocument = new XmlDocument();
            try
            {
                XDocument d = XDocument.Parse(xml);
                d.Root.Descendants().Attributes().Where(x => x.IsNamespaceDeclaration).Remove();

                foreach (var elem in d.Descendants())
                    elem.Name = elem.Name.LocalName;

                xmlDocument.Load(d.CreateReader());
            }
            catch (Exception ex)
            {
                throw;
            }

            return xmlDocument.Value;
        }

        public static XmlDocument stripDocumentNamespace(XmlDocument oldDom)
        {
            XmlDocument newDom = new XmlDocument();
            try
            {
                // Remove all xmlns:* instances from the passed XmlDocument
                // to simplify our xpath expressions.

                newDom.LoadXml(System.Text.RegularExpressions.Regex.Replace(
                oldDom.OuterXml, @"(xmlns:?[^=]*=[""][^""]*[""])", "",
                System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Multiline)
                );
            }
            catch (Exception ex)
            {
                throw;
            }
            return newDom;
        }

        //Core recursion function
        private static XElement RemoveAllNamespaces(XElement xmlDocument)
        {
            if (!xmlDocument.HasElements)
            {
                XElement xElement = new XElement(xmlDocument.Name.LocalName);
                xElement.Value = xmlDocument.Value;

                foreach (XAttribute attribute in xmlDocument.Attributes())
                    xElement.Add(attribute);

                return xElement;
            }
            return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
        }

        public static String RemoveDuplicates(String s)
        {
            StringBuilder noDupes = new StringBuilder();
            for (int i = 0; i < s.Length; i++)
            {
                String si = s.Substring(i, 1);
                if (noDupes.ToString().IndexOf(si) == -1)
                {
                    noDupes.Append(si);
                }
            }
            return noDupes.ToString();
        }

        public static void ReadFileLineByLineAndRemoveDuplicateChars(string filePath)
        {
            string line;
            try
            {
                // Read the file and display it line by line.
                System.IO.StreamReader file =
                   new System.IO.StreamReader(filePath);
                while ((line = file.ReadLine()) != null)
                {
                    Console.WriteLine(RemoveDuplicates(line));
                }

                file.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static void DeleteFileIfExists(string filePath)
        {
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        public static void DeleteFolderIfExists(string folderPath)
        {
            if (Directory.Exists(folderPath))
            {
                foreach (string directory in Directory.GetDirectories(folderPath))
                {
                    DeleteFolderIfExists(directory);
                }
                try
                {
                    Directory.Delete(folderPath, true);
                }
                catch (IOException)
                {
                    Directory.Delete(folderPath, true);
                }
                catch (UnauthorizedAccessException)
                {
                    Directory.Delete(folderPath, true);
                }
            }
        }

        public static void DeleteFilesOfCertainExtensions(string folderPath, string extension)
        {
            try
            {
                if (Directory.Exists(folderPath))
                {
                    if (folderPath.Length > 4) //Avoid deleting content of c:/
                    {
                        DirectoryInfo di = new DirectoryInfo(folderPath);
                        FileInfo[] files = di.GetFiles("*." + extension)
                                             .Where(p => p.Extension == "." + extension).ToArray();
                        foreach (FileInfo file in files)
                        {
                            file.Attributes = FileAttributes.Normal;
                            File.Delete(file.FullName);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static void DeleteFileStartingWith(string folderPath, string startWith, string extension)
        {
            string rootFolderPath = folderPath;
            try
            {
                if (Directory.Exists(folderPath))
                {
                    if (folderPath.Length > 4) //Avoid deleting content of c:/
                    {
                        if (!extension.Contains(".")) //Adding the point
                            extension = "." + extension;
                        string filesToDelete = startWith + "*" + extension;   // Only delete  files StartWith "DeleteMe" in their filenames
                        string[] fileList = System.IO.Directory.GetFiles(rootFolderPath, filesToDelete);
                        foreach (string file in fileList)
                        {
                            System.Diagnostics.Debug.WriteLine(file + "will be deleted");
                            System.IO.File.Delete(file);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static string GetLineFromFile(string fileName, int line)
        {
            using (var sr = new StreamReader(fileName))
            {
                for (int i = 1; i < line; i++)
                    sr.ReadLine();
                return sr.ReadLine();
            }
        }

        public static string GetLineFromString(string text, int lineNo)
        {
            string[] lines = text.Replace("\r", "").Split('\n');
            return lines.Length >= lineNo ? lines[lineNo - 1] : null;
        }

        public static string GetLinesRangeFromString(string text, int fromLineNo, int toLineNo)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                for (int i = fromLineNo; i <= toLineNo; i++)
                {
                    sb.AppendLine(GetLineFromString(text, i));
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return sb.ToString();
        }

        public static string ReplaceInvalidCharacters(string str, string replaceWith)
        {
            return str.Replace(";", replaceWith).Replace("\\", replaceWith).Replace("/", replaceWith).Replace(":", replaceWith).Replace("*", replaceWith).Replace("?", replaceWith).Replace("\"", replaceWith).Replace("<", replaceWith).Replace(">", replaceWith).Replace("|", replaceWith).Replace("-", replaceWith).Replace(" ", replaceWith);
        }

        public static List<string> TakeLastLines(string text, int count)
        {
            List<string> lines = new List<string>();
            Match match = Regex.Match(text, "^.*$", RegexOptions.Multiline | RegexOptions.RightToLeft);

            while (match.Success && lines.Count < count)
            {
                lines.Insert(0, match.Value);
                match = match.NextMatch();
            }

            return lines;
        }

        public static string ListToCommaSeperatedList(List<string> list)
        {
            return ListToString(list,",");
        }

        private static string ListToString(List<string> list, string seperator)
        {
            return String.Join(seperator, list);
        }

        public static string TakeLastLine(string text)
        {
            List<string> lines = new List<string>();
            Match match = Regex.Match(text, "^.*$", RegexOptions.Multiline | RegexOptions.RightToLeft);

            while (match.Success && lines.Count < 1)
            {
                lines.Insert(0, match.Value);
                match = match.NextMatch();
            }

            return lines[0];
        }

        public static List<string> ReadAllFilesNamesInfolder(string folderPath, string ext)
        {
            List<string> filesPathList = new List<string>();
            ext = ext.Replace(".", "").ToLower(); //If we got the ext with the dot
            try
            {
                DirectoryInfo dir = new DirectoryInfo(folderPath);
                foreach (FileInfo flInfo in dir.GetFiles())
                {
                    if (flInfo.Extension.ToLower() == ext || flInfo.Extension.ToLower() == "." + ext)
                    {
                        String name = flInfo.Name;
                        filesPathList.Add(name);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return filesPathList;
        }

        public static string RemoveInvalidFilePathCharacters(string filename, string replaceChar)
        {
            Regex r;
            try
            {
                string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
                r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            }
            catch (Exception ex)
            {
                throw;
                return filename;
            }
            return r.Replace(filename, replaceChar);
        }

        public static bool IsFileLocked(string filePath)
        {
            FileInfo file = new FileInfo(filePath);
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (FileNotFoundException)
            {
                return false;
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        public static bool IsFileExists(string fullPath)
        {
            try
            {
                if (File.Exists(fullPath))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool IsDirectoryExists(string fullPath)
        {
            try
            {
                if (Directory.Exists(fullPath))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool DataTableToCsv(DataTable dt)
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                string[] columnNames = dt.Columns.Cast<DataColumn>().
                                                  Select(column => column.ColumnName).
                                                  ToArray();
                sb.AppendLine(string.Join(",", columnNames));

                foreach (DataRow row in dt.Rows)
                {
                    string[] fields = row.ItemArray.Select(field => field.ToString()).
                                                    ToArray();
                    sb.AppendLine(string.Join(",", fields));
                }

                string result = SaveFileToRunFolder(sb.ToString(), "duplicates", "csv");
                return true;
            }
            catch (Exception ex)
            {
                throw;
                return false;
            }

        }

        public static bool IsNumeric(string num)
        {
            int n;
            bool isNumeric = int.TryParse(num, out n);
            return isNumeric;
        }

        //returns the longest repeated substring in input string
        public static string LongestRepeatedSubstring(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return null;
            }

            int N = input.Length;
            string[] substrings = new string[N];

            //form an array of substring i to N
            for (int i = 0; i < N; i++)
            {
                substrings[i] = input.Substring(i);
            }

            //Sort the array
            Array.Sort(substrings);

            string result = "";
            for (int i = 0; i < N - 1; i++)
            {
                string lcp = LongestCommonString(substrings[i], substrings[i + 1]);
                if (lcp.Length > result.Length)
                {
                    result = lcp;
                }
            }
            return result;
        }

        //returns the longest common matching substring between two string
        public static string LongestCommonString(string a, string b)
        {
            int n = Math.Min(a.Length, b.Length);
            string result = "";
            for (int i = 0; i < n; i++)
            {
                if (a[i] == b[i])
                {
                    result = result + a[i];
                }
                else
                {
                    break;
                }
            }
            return result;
        }

        public static string GetProcessorInfo()
        {
            string processorInfo = "";
            try
            {
                RegistryKey processor_name = Registry.LocalMachine.OpenSubKey(@"Hardware\Description\System\CentralProcessor\0", RegistryKeyPermissionCheck.ReadSubTree);   //This registry entry contains entry for processor info.
                if (processor_name != null)
                {
                    if (processor_name.GetValue("ProcessorNameString") != null)
                    {
                        processorInfo = processor_name.GetValue("ProcessorNameString").ToString(); //Display processor ingo.
                        return processorInfo;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return "";
        }

        public static string IsOs64Bit()
        {

            try
            {
                return System.Environment.Is64BitOperatingSystem.ToString();
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }
        

        public static string GetLogicalProcessors()
        {
            return Environment.ProcessorCount.ToString();
        }

        public static string BuildUrl(string baseUrl, string extraUrl)
        {
            Uri result = null;

            if (Uri.TryCreate(new Uri(baseUrl), extraUrl, out result))
            {
                return result.ToString();
            }

            return "";
        }

        //Function to get random number
        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();
        public static int RandomNumber(int min, int max)
        {
            lock (syncLock)
            { // synchronize
                return random.Next(min, max);
            }
        }

        public static string GenerateRandomStringNumber(int numOfChars)
        {
      
            string id = DateTime.Now.Ticks.ToString();
            string randomId = id.Substring(Math.Max(0, id.Length - numOfChars));
            return randomId;        
        }

        public static string RemoveAllNonAlphanumericCharacters(string text, string replaceWith)
        {
            Regex rgx = new Regex("[^a-zA-Z0-9]");
            text = rgx.Replace(text, replaceWith);

            return text;
        }

        /// <summary>
        /// Sending GET request.
        /// </summary>
        /// <param name="Url">Request Url.</param>
        /// <param name="queryString">Data for request.</param>
        /// <returns>Response body.</returns>
        public static string HTTP_GET(string Url, string queryString, string contentType, Dictionary<string, string> headersDic = null)
        {
            string Out = String.Empty;
            System.Net.WebRequest req = System.Net.WebRequest.Create(Url + (string.IsNullOrEmpty(queryString) ? "" : "?" + queryString));
            try
            {
                req.ContentType = contentType;
                if (headersDic != null && headersDic.Count > 0)
                {
                    foreach (KeyValuePair<string, string> entry in headersDic)
                    {
                        req.Headers.Add(entry.Value, entry.Key);
                    }
                }
                System.Net.WebResponse resp = req.GetResponse();
                using (System.IO.Stream stream = resp.GetResponseStream())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(stream))
                    {
                        Out = sr.ReadToEnd();
                        sr.Close();
                    }
                }
            }
            catch (ArgumentException ex)
            {
                Out = string.Format("HTTP_ERROR :: The second HttpWebRequest object has raised an Argument Exception as 'Connection' Property is set to 'Close' :: {0}", ex.Message);
            }
            catch (WebException ex)
            {
                Out = string.Format("HTTP_ERROR :: WebException raised! :: {0}", ex.Message);
            }
            catch (Exception ex)
            {
                Out = string.Format("HTTP_ERROR :: Exception raised! :: {0}", ex.Message);
            }

            return Out;
        }

        /// <summary>
        /// Sending POST request.
        /// </summary>
        /// <param name="Url">Request Url.</param>
        /// <param name="Data">Data for request.</param>
        /// <returns>Response body.</returns>
        public static string HTTP_POST_PUT(string Url, string Data, string contentType, string method, Dictionary<string, string> headersDic = null)
        {
            string Out = String.Empty;
            System.Net.WebRequest req = System.Net.WebRequest.Create(Url);
            try
            {
                req.Method = method;
                req.Timeout = 100000;
                req.ContentType = contentType;
                if (headersDic != null && headersDic.Count > 0)
                {
                    foreach (KeyValuePair<string, string> entry in headersDic)
                    {
                        req.Headers.Add(entry.Value, entry.Key);
                    }
                }
                byte[] sentData = Encoding.UTF8.GetBytes(Data);
                req.ContentLength = sentData.Length;
                using (System.IO.Stream sendStream = req.GetRequestStream())
                {
                    sendStream.Write(sentData, 0, sentData.Length);
                    sendStream.Close();
                }
                System.Net.WebResponse res = req.GetResponse();
                System.IO.Stream ReceiveStream = res.GetResponseStream();
                using (System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8))
                {
                    Char[] read = new Char[256];
                    int count = sr.Read(read, 0, 256);

                    while (count > 0)
                    {
                        String str = new String(read, 0, count);
                        Out += str;
                        count = sr.Read(read, 0, 256);
                    }
                }
            }
            catch (ArgumentException ex)
            {
                Out = string.Format("HTTP_ERROR :: The second HttpWebRequest object has raised an Argument Exception as 'Connection' Property is set to 'Close' :: {0}", ex.Message);
            }
            catch (WebException ex)
            {
                Out = string.Format("HTTP_ERROR :: WebException raised! :: {0}", ex.Message);
            }
            catch (Exception ex)
            {
                Out = string.Format("HTTP_ERROR :: Exception raised! :: {0}", ex.Message);
            }

            return Out;
        }


    }

    public class ProcessResult
    {
        public string Result { get; set; }
        public bool IsError { get; set; }
    }
}
