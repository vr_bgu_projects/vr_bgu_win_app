﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Utilities.IO;


namespace vrbugwinapp.Helpers
{
    class ImageProcessingUtil
    {
        public static string GetNewlyCreatedTempFolderPath()
        {
            string tempFolderPath = Path.Combine(Util.GetExeApplicationPath(), "Temp", Util.GenerateRandomStringNumber(8));
            bool creationResult = Util.CreateFolder(tempFolderPath);
            if (creationResult)
            {
                return tempFolderPath;
            }
            return null;
        }


    }
}
