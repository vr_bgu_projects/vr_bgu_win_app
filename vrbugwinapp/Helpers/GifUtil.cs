﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vrbugwinapp.Helpers
{
    class GifUtil
    {

        public static List<string> SplitGifToFRames(string gifFilePath)
        {
            List<string> framesPathList = new List<string>();
            try
            {
                string tempFolderPath = ImageProcessingUtil.GetNewlyCreatedTempFolderPath();
                string processName = Path.GetFullPath(Path.Combine(Util.GetExeApplicationPath(), "convert.exe"));
                string args = string.Format("-coalesce -delete 0 -loop 0 \"{0}\" \"{1}/xx_%05d.png\"", Util.ReplaceToUnixSlashes(gifFilePath), Util.ReplaceToUnixSlashes(tempFolderPath));
                ProcessResult pr = Util.RunProcess(processName, args);
                if (!pr.IsError)
                {
                    string splitResult = Util.RunProcess(processName, args).Result;

                    string[] filePaths = Directory.GetFiles(tempFolderPath);
                    framesPathList.AddRange(filePaths);
                }
                else
                {
                    string message = string.Format("Error in running process {0} with arguments {1}\nProcess Errors: {2}", processName, args, pr.Result);
                    Logger.log.DebugFormat(message);
                }
            }
            catch (Exception ex)
            {

            }
            return framesPathList;
        }

        
    }
}
