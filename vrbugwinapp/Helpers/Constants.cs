﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace vrbugwinapp.Helpers
{
    public sealed class Constants
    {
        private static Constants instance = null;

        private static readonly object padlock = new object();

        Constants()
        { }

        public static Constants Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {

                        instance = new Constants();
                    }
                    return instance;
                }
            }
        }
        public static string ConnectionString { get; set; }
        public static string DestTableName { get; set; }
        public static string DestDBName { get; set; }
        public static List<string> MSOxfordVisionKeys { get; set; }
        public static string LogFilePath { get; set; }
        public static List<string> MSOxfordWebLMKeys { get; set; }
        public static int GifToFramesEvery { get; set; }
    }
}