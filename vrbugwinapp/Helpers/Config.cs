﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;
using vrbugwinapp.Helpers;
using System.Web;
using System.IO;

namespace vrbugwinapp.Helpers
{
    public sealed class Config
    {
        private static Config instance = null;

        private static readonly object padlock = new object();

        Config()
        {}

        public static Config Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {

                        instance = new Config();
                    }
                    return instance;
                }
            }
        }

        private static XmlDocument configuration = null;        

        public static string GetConfigFilePath()
        {
            var configFilePath = "";
            try
            {
                string appPath = Util.GetExeApplicationPath();
                configFilePath = Path.Combine(appPath, "vrbugwinapp.config");
                if (Util.IsFileExists(configFilePath))
                    return configFilePath;
                else
                    throw new FileNotFoundException(string.Format("Config File: '{0}' not found",configFilePath));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return configFilePath;
        }

        public static string GetConfigFileContent()
        {
            string fileContent = "";
            try
            {
                fileContent =File.ReadAllText(GetConfigFilePath());
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return fileContent;
        }

        public static string GetConfigurationValue(string filePath, string sectionnName, string settingName)
        {
            try
            {
                if (configuration == null)
                {
                    configuration = new XmlDocument();
                    string fileContent = System.IO.File.ReadAllText(filePath);
                    configuration.PreserveWhitespace = false;
                    configuration.LoadXml(fileContent);
                    //Logger.Log("Configuration file content: " + configuration.InnerXml);
                }
                XmlNode node = configuration.DocumentElement.SelectSingleNode("/configuration/" + sectionnName + "/" + settingName);
                return node.InnerText;
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
            }
            return "";
        }


        public static XmlNodeList GetConfigurationList(string filePath, string sectionnName, string settingName)
        {
            try
            {
                if (configuration == null)
                {
                    configuration = new XmlDocument();
                    string fileContent = System.IO.File.ReadAllText(filePath);
                    configuration.PreserveWhitespace = false;
                    configuration.LoadXml(fileContent);
                    Logger.log.Info("Configuration file content: " + configuration.InnerXml);
                }
                XmlNodeList nodes = configuration.DocumentElement.SelectNodes("/configuration/" + sectionnName + "/" + settingName);
                return nodes;
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
            }
            return null;
        }

        public static void LoadingConfiguration()
        {
            string configFilePath = GetConfigFilePath();
            Constants.LogFilePath = Config.GetConfigurationValue(configFilePath, "General", "LogFilePath");
            Constants.MSOxfordVisionKeys = Util.CommaSeperatedStringToList(Config.GetConfigurationValue(configFilePath, "MSOxford", "MSOxfordVisionKeys"));
            Constants.MSOxfordWebLMKeys = Util.CommaSeperatedStringToList(Config.GetConfigurationValue(configFilePath, "MSOxford", "MSOxfordWebLMKeys"));
            Constants.GifToFramesEvery = Convert.ToInt32(Config.GetConfigurationValue(configFilePath, "Image", "GifToFramesEvery"));
        }
    }
}
