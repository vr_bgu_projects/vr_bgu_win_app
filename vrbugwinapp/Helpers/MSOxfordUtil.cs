﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using vrbugwinapp.Contract;
using System.Dynamic;
using Newtonsoft.Json;
using System.Diagnostics;
using vrbugwinapp.MsOxford;
using vrbugwinapp.MsOxford.WebLM;
using System.Windows.Forms;
using log4net.Core;

namespace vrbugwinapp.Helpers
{
    class MSOxfordUtil
    {
        private MainForm mainForm = null;

        /// <summary>
        /// The service host
        /// </summary>
        private const string DEFAULT_API_ROOT = "https://api.projectoxford.ai/vision/v1.0";

        /// <summary>
        /// Host root, overridable by subclasses, intended for testing.
        /// </summary>
        protected virtual string ServiceHost => _apiRoot;

        /// <summary>
        /// Default timeout for calls
        /// </summary>
        private const int DEFAULT_TIMEOUT = 2 * 60 * 1000; // 2 minutes timeout

        /// <summary>
        /// Default timeout for calls, overridable by subclasses
        /// </summary>
        protected virtual int DefaultTimeout => DEFAULT_TIMEOUT;

        /// <summary>
        /// The analyze query
        /// </summary>
        private const string AnalyzeQuery = "analyze";


        /// <summary>
        /// The describe query
        /// </summary>
        private const string DescribeQuery = "describe";

        /// <summary>
        /// The models-based query path part
        /// </summary>
        private const string ModelsPart = "models";

        /// <summary>
        /// The generate thumbnails query
        /// </summary>
        private const string ThumbnailsQuery = "generateThumbnail";

        /// <summary>
        /// Query parameter for maximum description candidates.
        /// </summary>
        private const string _maxCandidatesName = "maxCandidates";
        /// <summary>
        /// The subscription key name
        /// </summary>
        private const string _subscriptionKeyName = "subscription-key";

        /// <summary>
        /// The default resolver
        /// </summary>
        private CamelCasePropertyNamesContractResolver _defaultResolver = new CamelCasePropertyNamesContractResolver();

        /// <summary>
        /// The subscription key
        /// </summary>
        private string _subscriptionKey;

        /// <summary>
        /// The root URI for Vision API
        /// </summary>
        private readonly string _apiRoot;

        public string GetVisionRandomKey()
        {
            int index = Util.RandomNumber(0, Constants.MSOxfordVisionKeys.Count - 1);            
            return Constants.MSOxfordVisionKeys[index];
        }

        public string GetWebLMRandomKey()
        {
            int index = Util.RandomNumber(0, Constants.MSOxfordWebLMKeys.Count - 1);
            return Constants.MSOxfordWebLMKeys[index];
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VisionServiceClient"/> class.
        /// </summary>
        /// <param name="_subscriptionKey">The subscription key.</param>
        public MSOxfordUtil(string subscriptionKey, string apiRoot)
        {
            _apiRoot = apiRoot?.TrimEnd('/');
            _subscriptionKey = subscriptionKey;
        }

        public MSOxfordUtil(Form callingForm)
        {
            mainForm = callingForm as MainForm;
        }

        public DescribeImageResult DescribeImageByUrlCurl(string APIKey, string imageUrl, string maxCandidates)
        {
            DescribeImageResult describeImageResponse = null;
            string result = "";
            try
            {
                if (string.IsNullOrEmpty(maxCandidates))
                {
                    maxCandidates = "1";
                }

                string body = "{'url':'" + imageUrl + "'}";

                Stopwatch stopwatch = new Stopwatch();
                string tagArguments = string.Format(" -v -X POST \"https://api.projectoxford.ai/vision/v1.0/describe?maxCandidates={0}\" -H \"Content-Type: application/json\" -H \"Ocp-Apim-Subscription-Key: {1}\" --data-ascii \"{2}\"", maxCandidates, APIKey, body);
                string message = string.Format("Runing curl.exe with the arguments {0}", tagArguments);
                mainForm.AddToLog(message, Level.Debug);
                stopwatch.Start();
                result = Util.RunProcess("curl.exe", tagArguments).Result;
                stopwatch.Stop();
                mainForm.AddToLog(String.Format("Result:{1}{2}Time Elapsed: {0} ms", stopwatch.Elapsed.TotalMilliseconds, result, Environment.NewLine), Level.Debug);

                describeImageResponse = JsonConvert.DeserializeObject<DescribeImageResult>(result);
            }
            catch (Exception ex)
            {
                mainForm.AddToLog(ex.ToString(), Level.Error);
            }
            return describeImageResponse;
        }


        public DescribeImageResult DescribeImageByPathCurl(string APIKey, string imagePath, string maxCandidates)
        {
            DescribeImageResult describeImageResponse = null;
            string result = "";
            try
            {
                if (string.IsNullOrEmpty(maxCandidates))
                {
                    maxCandidates = "1";
                }
                Stopwatch stopwatch = new Stopwatch();
                string tagArguments = string.Format(" -v -X POST \"https://api.projectoxford.ai/vision/v1.0/describe?maxCandidates={0}\" -H \"Content-Type:application/octet-stream\" -H \"Ocp-Apim-Subscription-Key:{1}\" --data-binary \"@{2}\"", maxCandidates, APIKey, imagePath);
                string message = string.Format("Runing curl.exe with the arguments {0}", tagArguments);
                mainForm.AddToLog(message, Level.Debug);
                stopwatch.Start();
                result = Util.RunProcess("curl.exe", tagArguments).Result;
                stopwatch.Stop();
                mainForm.AddToLog(String.Format("Result:{1}{2}Time Elapsed: {0} ms", stopwatch.Elapsed.TotalMilliseconds, result,Environment.NewLine),Level.Debug);

                describeImageResponse = JsonConvert.DeserializeObject<DescribeImageResult>(result);
            }
            catch (Exception ex)
            {
                mainForm.AddToLog(ex.ToString(), Level.Error);
            }
            return describeImageResponse;
        }


        public JointProbabilityResponse WebMlCalculateJointProbabilityCurl(string APIKey, string[] queries)
        {
            if ((queries == null) || (queries.Length == 0))
                throw new ArgumentException("Arguments must be valid.");

            JointProbabilityResponse jointProbabilityResponse = null;
            string result = "";
            try
            {
                Stopwatch stopwatch = new Stopwatch();
                string body = "{'queries':['" + queries[0] + "']}";
                string tagArguments = string.Format(" -v -X POST \"https://api.projectoxford.ai/text/weblm/v1.0/calculateJointProbability?model=body\" -H \"Content-Type:application/json\" -H \"Ocp-Apim-Subscription-Key:{0}\" --data-ascii \"{1}\"",  APIKey, body);
                string message = string.Format("Runing curl.exe with the arguments {0}", tagArguments);
                mainForm.AddToLog(message, Level.Debug);

                stopwatch.Start();
                result = Util.RunProcess("curl.exe", tagArguments).Result;
                stopwatch.Stop();
                mainForm.AddToLog(String.Format("Result:{1}{2}Time Elapsed: {0} ms", stopwatch.Elapsed.TotalMilliseconds, result, Environment.NewLine), Level.Debug);

                jointProbabilityResponse = JsonConvert.DeserializeObject<JointProbabilityResponse>(result);
            }
            catch (Exception ex)
            {
                mainForm.AddToLog(ex.ToString(), Level.Error);
            }
            return jointProbabilityResponse;
        }
    }
}
