﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace vrbugwinapp.Helpers
{
    public class VideoUtil
    {
        private MainForm mainForm = null;

        public VideoUtil(Form callingForm)
        {
            mainForm = callingForm as MainForm;
        }

        public List<string> TakeKeyFrames(string videoFilePath)
        {
            List<string> framesPathList = new List<string>();
            try
            {
                string tempFolderPath = ImageProcessingUtil.GetNewlyCreatedTempFolderPath();
                //string processPath = Path.GetFullPath(Path.Combine(Util.GetExeApplicationPath(), "ffmpeg.exe"));
                //string args = "-i \""+ videoFilePath + "\" -vf \"select=eq(pict_type\\,I)\" -vsync vfr \""+ tempFolderPath + "\\image%%03d.jpg\"";
                mainForm.AddToLog("Created temp folder for moviw images: " + tempFolderPath);

                string processPath = Path.GetFullPath(Path.Combine(Util.GetExeApplicationPath(), "ffmpeg.bat"));

                string batFileContent = File.ReadAllText(processPath);
                batFileContent = batFileContent.Replace("${input}", videoFilePath);
                batFileContent = batFileContent.Replace("${output}", tempFolderPath);
                string newBatFilePath = Path.GetFullPath(Path.Combine(Util.GetExeApplicationPath(), "ffmpeg_bak.bat"));
                File.WriteAllText(newBatFilePath, batFileContent);
                processPath = Path.GetFullPath(Path.Combine(Util.GetExeApplicationPath(), newBatFilePath));


                //string args = videoFilePath + " " -vf \"select=eq(pict_type\\,I)\" -vsync vfr \"" + tempFolderPath + "\\image%%03d.jpg\"";
                string args = "";
                ProcessResult pr = Util.RunProcess2(processPath, args);

                //ProcessResult pr = Util.RunProcess(processPath, args);
                //if (!pr.IsError)
                //{
                //string splitResult = Util.RunProcess(processPath, args).Result;
                string[] filePaths = Directory.GetFiles(tempFolderPath);
                framesPathList.AddRange(filePaths);
                //}
                //else
                //{
                //string message = splitResult;
                Logger.log.DebugFormat(pr.Result);
                //}
            }
            catch (Exception ex)
            {

            }
            return framesPathList;
        }
    }
}
