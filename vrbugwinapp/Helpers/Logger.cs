﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using vrbugwinapp.Helpers;
using System.Threading.Tasks;
using System.Threading;
using log4net;
using log4net.Core;

namespace vrbugwinapp.Helpers
{
    public static class Logger
    {
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public static bool CleanLog()
        {
            bool result = false;
            try
            {
                string logFilePath = Constants.LogFilePath;
                if (File.Exists(logFilePath))
                {
                    File.WriteAllText(Constants.LogFilePath, "");
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex.ToString());
                File.WriteAllText(Constants.LogFilePath, "");

            }

            return result;
        }

        public static void LogIncomingHttpContentMessageAsDebug(dynamic data, string openingMessage)
        {
            if (data != null && !string.IsNullOrEmpty(data.ToString()))
            {
                string message = String.Format("{0}\n{1}", openingMessage, data.ToString());
                Task.Run(() => LogMessageToDebug(message));
            }
        }

        public static void LogMessageToDebug(string message)
        {
            log.Debug(message);
        }

        public static bool Log(Level level, string message, Exception exception = null)
        {
            var logger = log.Logger;
            if (logger.IsEnabledFor(level))
            {
                logger.Log(logger.GetType(), level, message, exception);
                return true;
            }

            return false;
        }

        public static bool LogFormat(Level level, string messageFormat, params object[] messageArguments)
        {
            var logger = log.Logger;
            if (logger.IsEnabledFor(level))
            {
                var message = string.Format(messageFormat, messageArguments);
                logger.Log(logger.GetType(), level, message, exception: null);

                return true;
            }

            return false;
        }
    }
}
