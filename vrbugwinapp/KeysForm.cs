﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using vrbugwinapp.Helpers;

namespace vrbugwinapp
{
    public partial class KeysForm : Form
    {
        public KeysForm()
        {
            InitializeComponent();
            LoadAllVisionKeys();
            LoadAllWebLMKeys();
        }

        private void LoadAllWebLMKeys()
        {
            StringBuilder sb = new StringBuilder();
            //MsOxford
            int numbering = 1;
            richTextBoxKeys.AppendText("MS Oxford Web LM Keys\n----------------\n");
            foreach (string key in Constants.MSOxfordWebLMKeys)
            {
                sb.AppendFormat("{0}. {1}\n", numbering, key);
                numbering++;
            }
            richTextBoxKeys.AppendText(sb.ToString());
            richTextBoxKeys.AppendText(Environment.NewLine);

        }

        private void LoadAllVisionKeys()
        {
            StringBuilder sb = new StringBuilder();
            //MsOxford
            int numbering = 1;
            richTextBoxKeys.AppendText("MS Oxford Vision Keys\n----------------\n");
            foreach (string key in Constants.MSOxfordVisionKeys)
            {
                sb.AppendFormat("{0}. {1}\n", numbering, key);
                numbering++;
            }
            richTextBoxKeys.AppendText(sb.ToString());
            richTextBoxKeys.AppendText(Environment.NewLine);
        }
    }
}
