﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using vrbugwinapp.Helpers;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Web;
using System.IO;
using Microsoft.ProjectOxford.Vision;
using Microsoft.ProjectOxford.Vision.Contract;
using vrbugwinapp.MsOxford;
using System.Threading;
using vrbugwinapp.MsOxford.WebLM;
using log4net.Core;

namespace vrbugwinapp
{
    public partial class ImageDetails : UserControl
    {
        public string imgPath { get; set; }
        public string imgNumber { get; set; }
        public string imgName { get; set; }
        public string imgDescription { get; set; }
        public double imgDescriptionConfidence { get; set; }
        public List<string> listOfTagsPerImage { get; set; }
        private MainForm mainForm = null;

        public ImageDetails(string imagePath, string imageName, string imageNumber, Form callingForm)
        {
            mainForm = callingForm as MainForm;
            InitializeComponent();
            imgPath = imagePath;
            imgName = imageName;
            imgNumber = imageNumber;
            PopulateImageDetails();
        }

        private void PopulateImageDetails()
        {
            pictureBoxImage.Image = Image.FromFile(imgPath);
            pictureBoxImage.SizeMode = PictureBoxSizeMode.StretchImage;
            labelImage.Text = imgName;
            labelNumber.Text = imgNumber;
            AddContextMenu();
        }

        private void AddContextMenu()
        {
            ContextMenu cm = new ContextMenu();            
            cm.MenuItems.Add("Describe Image", new System.EventHandler(DescribeImage));
            cm.MenuItems.Add("Calculate Joint Probability", new System.EventHandler(CalculateJointProbability));

            pictureBoxImage.ContextMenu = cm;
        }

        public async void DescribeImage(object sender, EventArgs e)
        {
            MSOxfordUtil msOXUtil = new MSOxfordUtil(mainForm);
            if (File.Exists(imgPath))
            {                
                string key = msOXUtil.GetVisionRandomKey();

                //string ext = Path.GetExtension(imgPath).Trim('.');
                //string fileCopyName = "temp." + ext;
                //File.Copy(imgPath, fileCopyName, true); // overwrite = true
                //using (FileStream stream = File.Open(fileCopyName, FileMode.Open))
                //{

                    //VisionServiceClient vc = new VisionServiceClient(key);

                    try
                    {
                    //AnalysisResult ar = vc.DescribeAsync(stream).Result;
                    DescribeImageResult dir = await Task.Run(() => msOXUtil.DescribeImageByPathCurl(key, imgPath, "1"));
                    if (dir != null && dir.description != null && dir.description.captions.Count > 0)
                    {
                        PopulateImageDesription(dir);
                        PopulateImageTags(dir);
                    }
                }
                catch (Exception ex)
                {
                    mainForm.AddToLog(ex.ToString(), Level.Error);
                }
            //}
                //File.Delete(fileCopyName);
            }

        }

        private void PopulateImageTags(DescribeImageResult dir)
        {
            if (dir != null && dir.description != null && dir.description.tags.Count > 0)
            {
                listOfTagsPerImage = dir.description.tags;
            }
        }

        private void PopulateImageDesription(DescribeImageResult dir)
        {
            imgDescription = dir.description.captions[0].text;
            imgDescriptionConfidence = dir.description.captions[0].confidence;
            textBoxImageDescription.Text = string.Format("<{0}>{1}{2}", imgDescriptionConfidence, Environment.NewLine, imgDescription);
        }

        private void buttonGetDescription_Click(object sender, EventArgs e)
        {
          
        }


        public async void CalculateJointProbability(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(imgDescription))
            {
                MSOxfordUtil msOXUtil = new MSOxfordUtil(mainForm);

                string key = msOXUtil.GetWebLMRandomKey();
                try
                {
                    //AnalysisResult ar = vc.DescribeAsync(stream).Result;
                    JointProbabilityResponse jpr = await Task.Run(() => msOXUtil.WebMlCalculateJointProbabilityCurl(key, new string[] { imgDescription }));
                    if (jpr != null && jpr.Results != null && jpr.Results.Length > 0)
                    {
                        labelJointProb.Visible = true;
                        labelJointProb.Text = jpr.Results[0].Probability.ToString();
                    }
                }
                catch (Exception ex)
                {
                    mainForm.AddToLog(ex.ToString(), Level.Error);
                }
                //}
                //File.Delete(fileCopyName);
            }

        }


        static async void MakeRequest(string imagePath)
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);

            // Request headers
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", "{subscription key}");

            // Request parameters
            queryString["visualFeatures"] = "Categories";
            //queryString["details"] = "{string}";
            queryString["language"] = "en";
            var uri = "https://api.projectoxford.ai/vision/v1.0/analyze?" + queryString;

            HttpResponseMessage response;

            string body = "{'url':'" + imagePath + "'}";

            // Request body
            byte[] byteData = Encoding.UTF8.GetBytes("{body}");

            using (var content = new ByteArrayContent(byteData))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                response = await client.PostAsync(uri, content);
            }

        }

      

        private void pictureBoxImage_MouseHover(object sender, EventArgs e)
        {
            if (listOfTagsPerImage != null)
            {
                //MessageBox.Show(Util.ListToCommaSeperatedList(this.listOfTagsPerImage));
                mainForm.SetPanelRightTagsContent(this.imgNumber, listOfTagsPerImage);
            }

        }
    }
}
