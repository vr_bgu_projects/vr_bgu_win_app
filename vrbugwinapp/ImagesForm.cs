﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using vrbugwinapp.Helpers;
using vrbugwinapp.Properties;

namespace vrbugwinapp
{
    public partial class ImagesForm : Form
    {
        public List<string> ImagesList = null;
        public ImagesForm()
        {
            InitializeComponent();
            //object O = Resources.ResourceManager.GetObject("hex_loader2"); //Return an object from the image chan1.png in the project
            //PictureBox pic = new PictureBox();
            //pic.Image = (Image)O;
            //this.Controls.Add(pic);
        }
        private MainForm mainForm = null;

        public ImagesForm(Form callingForm)
        {
            mainForm = callingForm as MainForm;
            InitializeComponent();
        }

        public void HideLoadingImage()
        {
            pictureBoxLoading.Visible = false;
        }

        public void LoadGifImages(List<string> imagesList)
        {
            int gifToFramesEvery = Constants.GifToFramesEvery;
            List<string> selectedGifFrames = new List<string>();
            if (imagesList != null && imagesList.Count > 0)
            {
                for(int i=0; i< imagesList.Count; i++)
                {
                    if (i % gifToFramesEvery != 0)
                        continue;
                    else
                        selectedGifFrames.Add(imagesList[i]);
                }
                LoadFrames(selectedGifFrames);
                string message = string.Format("Loaded {0} frames out of {1} frames of the gif, selecting every {2}th frame.",selectedGifFrames.Count, imagesList.Count, gifToFramesEvery);
                this.mainForm.AddToLog(message);
            }
        }

        public void LoadMp4Frames(List<string> imagesList)
        {
            if (imagesList != null && imagesList.Count > 0)
            {                
                LoadFrames(imagesList);
                string message = string.Format("Loaded {0} frames from the movie.", imagesList.Count);
                this.mainForm.AddToLog(message);
            }
        }

        public void LoadFrames(List<string> imagesList)
        {
            if (imagesList != null && imagesList.Count > 0)
            {
                int numbering = 1;

                int x = 30; int y = 30; int maxHeight = -1;
                for (int i = 0; i < imagesList.Count; i++)
                {                    
                    string imgFile = imagesList[i];
                    string imgName = Path.GetFileName(imgFile);
                    ImageDetails imgDet = new ImageDetails(imgFile, imgName, numbering.ToString(), mainForm);
                    imgDet.Location = new Point(x, y);
                    x += imgDet.Width + 10;
                    maxHeight = Math.Max(imgDet.Height, maxHeight);
                    if (x > this.ClientSize.Width - 100)
                    {
                        x = 30;
                        y += maxHeight + 10;
                    }
                    this.panelImages.Controls.Add(imgDet);
                    numbering++;
                }
            }
        }

        public void ProcessGifImage(string imagePath)
        {
            List<string> imgFiles = null;          
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += (s, e) => {
                imgFiles = GifUtil.SplitGifToFRames(imagePath);
                ImagesList = imgFiles;
            };
            bw.RunWorkerCompleted += (s, e) => {
                LoadGifImages(this.ImagesList);
                HideLoadingImage();
            };

            bw.RunWorkerAsync();
        }

        public void ProcessMp4Movie(string mediaPath)
        {
            List<string> imgFiles = null;
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += (s, e) => {
                VideoUtil vu = new VideoUtil(mainForm);
                imgFiles = vu.TakeKeyFrames(mediaPath);
                ImagesList = imgFiles;
            };
            bw.RunWorkerCompleted += (s, e) => {
                LoadMp4Frames(this.ImagesList);
                HideLoadingImage();
            };

            bw.RunWorkerAsync();
        }

        public void ProcessImage(string mediaPath)
        {
            throw new NotImplementedException();
        }

        private async void describeAllImagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < panelImages.Controls.Count; i++)
            {
                if (panelImages.Controls[i] is ImageDetails)
                {
                    //await Task.Run(() => ();
                    ((ImageDetails)panelImages.Controls[i]).DescribeImage(sender, e);
                    //Thread.Sleep(2000);

                }
            }
        }

        private void calculateJointProbabilityForAllDescriptionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < panelImages.Controls.Count; i++)
            {
                if (panelImages.Controls[i] is ImageDetails)
                {
                    //await Task.Run(() => ();
                    ((ImageDetails)panelImages.Controls[i]).CalculateJointProbability(sender, e);
                    //Thread.Sleep(2000);

                }
            }
        }
    }
}
