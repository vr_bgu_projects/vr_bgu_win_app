﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using vrbugwinapp.Helpers;

namespace vrbugwinapp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Config.LoadingConfiguration();
            Application.Run(new MainForm());
            
        }
    }
}
