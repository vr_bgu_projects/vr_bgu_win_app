﻿namespace vrbugwinapp
{
    partial class ImageDetails
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelImage = new System.Windows.Forms.Label();
            this.pictureBoxImage = new System.Windows.Forms.PictureBox();
            this.textBoxImageDescription = new System.Windows.Forms.TextBox();
            this.labelJointProb = new System.Windows.Forms.Label();
            this.labelNumber = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).BeginInit();
            this.SuspendLayout();
            // 
            // labelImage
            // 
            this.labelImage.AutoSize = true;
            this.labelImage.BackColor = System.Drawing.Color.Transparent;
            this.labelImage.Location = new System.Drawing.Point(3, 0);
            this.labelImage.Name = "labelImage";
            this.labelImage.Size = new System.Drawing.Size(76, 17);
            this.labelImage.TabIndex = 1;
            this.labelImage.Text = "labelImage";
            // 
            // pictureBoxImage
            // 
            this.pictureBoxImage.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxImage.Name = "pictureBoxImage";
            this.pictureBoxImage.Size = new System.Drawing.Size(200, 138);
            this.pictureBoxImage.TabIndex = 0;
            this.pictureBoxImage.TabStop = false;
            this.pictureBoxImage.MouseHover += new System.EventHandler(this.pictureBoxImage_MouseHover);
            // 
            // textBoxImageDescription
            // 
            this.textBoxImageDescription.Location = new System.Drawing.Point(3, 147);
            this.textBoxImageDescription.Multiline = true;
            this.textBoxImageDescription.Name = "textBoxImageDescription";
            this.textBoxImageDescription.ReadOnly = true;
            this.textBoxImageDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxImageDescription.Size = new System.Drawing.Size(200, 71);
            this.textBoxImageDescription.TabIndex = 3;
            // 
            // labelJointProb
            // 
            this.labelJointProb.AutoSize = true;
            this.labelJointProb.BackColor = System.Drawing.Color.Transparent;
            this.labelJointProb.ForeColor = System.Drawing.Color.Red;
            this.labelJointProb.Location = new System.Drawing.Point(136, 192);
            this.labelJointProb.Name = "labelJointProb";
            this.labelJointProb.Size = new System.Drawing.Size(24, 17);
            this.labelJointProb.TabIndex = 4;
            this.labelJointProb.Text = "##";
            this.labelJointProb.Visible = false;
            // 
            // labelNumber
            // 
            this.labelNumber.AutoSize = true;
            this.labelNumber.BackColor = System.Drawing.Color.White;
            this.labelNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumber.ForeColor = System.Drawing.Color.Navy;
            this.labelNumber.Location = new System.Drawing.Point(183, 8);
            this.labelNumber.Name = "labelNumber";
            this.labelNumber.Size = new System.Drawing.Size(17, 18);
            this.labelNumber.TabIndex = 5;
            this.labelNumber.Text = "#";
            // 
            // ImageDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.labelNumber);
            this.Controls.Add(this.labelJointProb);
            this.Controls.Add(this.textBoxImageDescription);
            this.Controls.Add(this.labelImage);
            this.Controls.Add(this.pictureBoxImage);
            this.Name = "ImageDetails";
            this.Size = new System.Drawing.Size(213, 221);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxImage;
        private System.Windows.Forms.Label labelImage;
        private System.Windows.Forms.TextBox textBoxImageDescription;
        private System.Windows.Forms.Label labelJointProb;
        private System.Windows.Forms.Label labelNumber;
    }
}
