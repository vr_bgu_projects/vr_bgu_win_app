﻿using log4net.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using vrbugwinapp.Helpers;
using Microsoft.WindowsAPICodePack.Shell.PropertySystem;
using Microsoft.WindowsAPICodePack.Shell;
using System.Globalization;

namespace vrbugwinapp
{
    public partial class MainForm : Form
    {
        private ContextMenuStrip listboxContextMenu;
        public Thread m_UIThread { get; set; }
        private bool textBoxMediaPathAlreadyFocused;
        public List<string> ImagesList = null;
        FormWindowState LastWindowState = FormWindowState.Minimized;
        public enum MediaType { IMAGE, VIDEO, NONE}
        


        public MainForm()
        {
            InitializeComponent();
            DoInitalStuff();
            //var x = GetShell32NameSpaceFolder();
        }

        private void DoInitalStuff()
        {
            //assign a contextmenustrip
            listboxContextMenu = new ContextMenuStrip();
            listboxContextMenu.Opening += new CancelEventHandler(listboxContextMenu_Opening);
            listBoxLog.ContextMenuStrip = listboxContextMenu;

            m_UIThread = Thread.CurrentThread;
            HideLoadingImage();
        }

        private void LoadDefaultGifImage()
        {
            ShowImageViewer();
            string file = "cat_tablet.gif";
            pictureBoxImage.Image = Image.FromFile(file);
            pictureBoxImage.SizeMode = PictureBoxSizeMode.StretchImage;
            ShowFileInformation(file);
        }

        private void ShowFileInformation(string file, MediaType mediaType = MediaType.IMAGE)
        {
            string fullFilePath = Util.GetFullFilePath(file);
            textBoxMediaPath.Visible = true;
            textBoxMediaPath.Text = fullFilePath;
            BackgroundWorker bw = new BackgroundWorker();
            string fileInfo = fullFilePath; //Default            
            bw.DoWork += (s, e) => {
                if (mediaType == MediaType.IMAGE)
                    fileInfo = GetImageInformation(fullFilePath);
                if (mediaType == MediaType.VIDEO)
                    fileInfo = GetVideoInformation(fullFilePath);
            };
            bw.RunWorkerCompleted += (s, e) =>
            {
                labelMediaInfo.Text = fileInfo;
                panelMediaInfo.Visible = true;
            };
            bw.RunWorkerAsync();
        }

        private string GetVideoInformation(string fullFilePath)
        {
            throw new NotImplementedException();
        }

        private string GetImageInformation(string file)
        {
            var sb = new StringBuilder();

            //List<string> arrHeaders = new List<string>();

            //Shell32.Shell shell = new Shell32.Shell();
            //var strFileName = file;
            //Shell32.Folder objFolder = shell.NameSpace(System.IO.Path.GetDirectoryName(strFileName));
            //Shell32.FolderItem folderItem = objFolder.ParseName(System.IO.Path.GetFileName(strFileName));

            //for (int i = 0; i < short.MaxValue; i++)
            //{
            //    string header = objFolder.GetDetailsOf(null, i);
            //    if (String.IsNullOrEmpty(header))
            //        break;
            //    arrHeaders.Add(header);
            //}

            //for (int i = 0; i < arrHeaders.Count; i++)
            //{
            //    sb.AppendFormat("{ 0}\t{ 1}: { 2}", i, arrHeaders[i], objFolder.GetDetailsOf(folderItem, i));
            //}

            ShellObject picture = ShellObject.FromParsingName(file);

            if (picture != null)
            {
                var imgDimensions = GetValue(picture.Properties.GetProperty(SystemProperties.System.Image.Dimensions));
                var imgBitDepth = GetValue(picture.Properties.GetProperty(SystemProperties.System.Image.BitDepth));
                var imgResoultion = GetValue(picture.Properties.GetProperty(SystemProperties.System.Image.ResolutionUnit));
                //sb.AppendFormat("Image{0}------{0}Dimension: {1}{0}Bit Depth: {2}{0}", Environment.NewLine, imgDimensions,imgBitDepth,imgResoultion);
                sb.AppendFormat("Dimension: {1}{0}Bit Depth: {2}{0}", Environment.NewLine, imgDimensions, imgBitDepth, imgResoultion);

                var fileName = GetValue(picture.Properties.GetProperty(SystemProperties.System.FileName));
                var itemType = GetValue(picture.Properties.GetProperty(SystemProperties.System.ItemType));

                var fileDateCreated = GetValue(picture.Properties.GetProperty(SystemProperties.System.DateCreated));
                var fileDateModifies = GetValue(picture.Properties.GetProperty(SystemProperties.System.DateModified));
                var fileSize = new FileInfo(fileName).Length;//ToSizeString(Convert.ToDouble(GetValue(picture.Properties.GetProperty(SystemProperties.System.Size))));
                var fileOwner = GetValue(picture.Properties.GetProperty(SystemProperties.System.FileOwner));
                //sb.AppendFormat("{0}File{0}------{0}Name: {1}{0}Type: {2}{0}Date Created: {3}{0}Date Modified: {4}{0}Size: {5}{0}Owner: {6}", Environment.NewLine, fileName, itemType, fileDateCreated, fileDateModifies, fileSize, fileOwner);
                sb.AppendFormat("Name: {1}{0}Type: {2}{0}Date Created: {3}{0}Date Modified: {4}{0}Size: {5} Bytes{0}Owner: {6}", Environment.NewLine, fileName, itemType, fileDateCreated, fileDateModifies, fileSize, fileOwner);

            }



            return sb.ToString();

        }

        public static string ToSizeString(double bytes)
        {
            var culture = CultureInfo.CurrentUICulture;
            const string format = "#,0.0";

            if (bytes < 1024)
                return bytes.ToString("#,0", culture);
            bytes /= 1024;
            if (bytes < 1024)
                return bytes.ToString(format, culture) + " KB";
            bytes /= 1024;
            if (bytes < 1024)
                return bytes.ToString(format, culture) + " MB";
            bytes /= 1024;
            if (bytes < 1024)
                return bytes.ToString(format, culture) + " GB";
            bytes /= 1024;
            return bytes.ToString(format, culture) + " TB";
        }

        private static string GetValue(IShellProperty value)
        {
            if (value == null || value.ValueAsObject == null)
            {
                return String.Empty;
            }
            return value.ValueAsObject.ToString();
        }

        public Shell32.Folder GetShell32NameSpaceFolder(Object folder)
        {
            Type shellAppType = Type.GetTypeFromProgID("Shell.Application");

            Object shell = Activator.CreateInstance(shellAppType);
            return (Shell32.Folder)shellAppType.InvokeMember("NameSpace",
            System.Reflection.BindingFlags.InvokeMethod, null, shell, new object[] { folder });
        }

        private void LoadDefaultMp4Movie()
        {            
            ShowVideoPlayer();
            string file = "demo.mp4";
            textBoxMediaPath.Text = Util.GetFullFilePath(file);
            axWindowsMediaPlayer1.URL = file;
        }
        
        private void ShowVideoPlayer()
        {
            pictureBoxImage.Visible = false;
            axWindowsMediaPlayer1.Visible = true;
            textBoxMediaPath.Visible = true;
        }

        private void ClearAll()
        {
            HideLoadingImage();
            ShowImageViewer();
            textBoxMediaPath.Text = "";
            textBoxMediaPath.Visible = false;
            panelMediaInfo.Visible = false;

            pictureBoxImage.Image =  Properties.Resources.multimedia_icon;            
            splitContainer1.Panel2.Controls.Clear();
            if(ImagesList != null)
                ImagesList.Clear();
            actionsToolStripMenuItem.Visible = false;

        }

        private void ShowImageViewer()
        {
            axWindowsMediaPlayer1.Visible = false;
            pictureBoxImage.Visible = true;
            textBoxMediaPath.Visible = true;
        }

        private void listboxContextMenu_Opening(object sender, CancelEventArgs e)
        {
            //clear the menu and add custom items
            listboxContextMenu.Items.Clear();
            ToolStripItem subItem = new ToolStripMenuItem();
            subItem.Click += MenuClicked;
            if (listBoxLog.SelectedItem != null)
                subItem.Tag = listBoxLog.SelectedItem.ToString();
            subItem.Text = "Copy";

            listboxContextMenu.Items.Add(subItem);
        }

        private void MenuClicked(object sender, EventArgs e)
        {
            string operationName = ((ToolStripMenuItem)sender).Text;
            if (operationName == "Copy")
            {
                string copiedText = ((ToolStripMenuItem)sender).Tag.ToString();
                Clipboard.SetText(copiedText);
            }
        }

        void menu_Click(object sender, EventArgs e)
        {
            MessageBox.Show(((MenuItem)sender).Text);
        }

        private void listBoxLog_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                //select the item under the mouse pointer
                listBoxLog.SelectedIndex = listBoxLog.IndexFromPoint(e.Location);
                if (listBoxLog.SelectedIndex != -1)
                {
                    listboxContextMenu.Show();
                }
            }
        }

        private void openImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenMedia();
        }

        private void buttonDescribeMedia_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBoxMediaPath.Text))
            {
                return;
            }
            string mediaPath = textBoxMediaPath.Text;
            string ext = Util.GetFileExtension(mediaPath).ToLower();
            string message = string.Format("Trying to describe media: {0}", mediaPath);
            flowLayoutPanelImages.Controls.Clear();
            //ImagesForm imagesForm = new ImagesForm(this);
            //imagesForm.Show();
            pictureBoxLoading.Show();

            AddToLog(message);
            switch(ext)
            {
                case "gif":
                    ProcessGifImage(mediaPath);
                    //imagesForm.ProcessGifImage(mediaPath);

                    return;
                case "mp4":
                    ProcessMp4Movie(mediaPath);
                    return;
                case "png":
                    //imagesForm.ProcessImage(mediaPath);
                    return;
                case "jpg":
                    //imagesForm.ProcessImage(mediaPath);
                    return;
                case "jpeg":
                    //imagesForm.ProcessImage(mediaPath);
                    return;
                default:
                   return;
            }
        }

        private void pictureBoxImage_Click(object sender, EventArgs e)
        {            
            OpenMedia();
        }

        private void OpenMedia()
        {
            openFileDialog1.Title = "Select Media File";
            openFileDialog1.FileName = "";
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                if (Util.IsRecognisedImageFile(file))
                {
                    pictureBoxImage.Visible = true;
                    axWindowsMediaPlayer1.Visible = false;
                    pictureBoxImage.Image = Image.FromFile(file);
                    pictureBoxImage.SizeMode = PictureBoxSizeMode.StretchImage;
                    ShowFileInformation(file, MediaType.IMAGE);
                    AddToLog("Image " + file +" has opened.");
                }
                else if (file.Contains("mp4"))
                {
                    textBoxMediaPath.Text = file;
                    pictureBoxImage.Visible = false;
                    axWindowsMediaPlayer1.Visible = true;
                    axWindowsMediaPlayer1.URL = file;
                    ShowFileInformation(file, MediaType.VIDEO);

                    //VideoPlayer vp = new VideoPlayer(file, pictureBoxImage);
                    //vp.Play();
                }
                else
                    MessageBox.Show("Not Recognised Image File", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error);


            }
            Console.WriteLine(result);
        }

        private void aPIKeysToolStripMenuItem_Click(object sender, EventArgs e)
        {
            KeysForm keysForm = new KeysForm();
            keysForm.Show();
        }

        private void loadMediaUrlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // This test that the InputBox can handle more newline than one.
            InputBoxResult mediaUrlBox = InputBox.Show("Enter a valid media url"
                          , "Load Media Url", "", 300, 0);

            if (mediaUrlBox.ReturnCode == DialogResult.OK)
            // MessageBox.Show(mediaUrlBox.Text);
            {
                try
                {
                    var request = WebRequest.Create(mediaUrlBox.Text);

                    using (var response = request.GetResponse())
                    using (var stream = response.GetResponseStream())
                    {
                        pictureBoxImage.Image = Bitmap.FromStream(stream);
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }
                //pictureBoxImage.ImageLocation = (mediaUrlBox.Text);
            }
        }

        private delegate void InvokeDelegate();

        public void AddToLog(String message, Level level = null )
        {
            if (Thread.CurrentThread != m_UIThread)
            {
                // Need for invoke if called from a different thread
                this.BeginInvoke(new InvokeDelegate(() => this.AddLineToLog(message, level)));
                    //DispatcherPriority.Normal, (ThreadStart)delegate ()
                    //{
                    //    AddToLog(message);
                    //});
            }
            else
            {
                AddLineToLog(message, level);
            }
        }

        private void  AddLineToLog(string message, Level logLevel)
        {
            if (logLevel == null)
                Logger.log.Debug(message);
            else
                Logger.Log(logLevel, message);
            // add this line at the top of the log
            listBoxLog.Items.Insert(0, message);

            // keep only a few lines in the log
            while (listBoxLog.Items.Count > 100)
            {
                listBoxLog.Items.RemoveAt(listBoxLog.Items.Count - 1);
            }
        }

        private void gifExampleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadDefaultGifImage();
        }

        private void mp4ExampleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadDefaultMp4Movie();
        }

        void textBoxMediaPath_Leave(object sender, EventArgs e)
        {
            textBoxMediaPathAlreadyFocused = false;
        }

        void textBoxMediaPath_GotFocus(object sender, EventArgs e)
        {
            // Select all text only if the mouse isn't down.
            // This makes tabbing to the textbox give focus.
            if (MouseButtons == MouseButtons.None)
            {
                this.textBoxMediaPath.SelectAll();
                textBoxMediaPathAlreadyFocused = true;
            }
        }

        private void textBoxMediaPath_MouseUp(object sender, MouseEventArgs e)
        {
            // Web browsers like Google Chrome select the text on mouse up.
            // They only do it if the textbox isn't already focused,
            // and if the user hasn't selected all text.
            if (!textBoxMediaPathAlreadyFocused && this.textBoxMediaPath.SelectionLength == 0)
            {
                textBoxMediaPathAlreadyFocused = true;
                this.textBoxMediaPath.SelectAll();
            }
        }

        private void textBoxMediaPath_Enter(object sender, EventArgs e)
        {
            // Kick off SelectAll asyncronously so that it occurs after Click
            BeginInvoke((Action)delegate
            {
                textBoxMediaPath.SelectAll();
            });
        }

        public void HideLoadingImage()
        {
            pictureBoxLoading.Visible = false;
        }

        public void LoadGifImages(List<string> imagesList)
        {
            int gifToFramesEvery = Constants.GifToFramesEvery;
            List<string> selectedGifFrames = new List<string>();
            if (imagesList != null && imagesList.Count > 0)
            {
                for (int i = 0; i < imagesList.Count; i++)
                {
                    if (i % gifToFramesEvery != 0)
                        continue;
                    else
                        selectedGifFrames.Add(imagesList[i]);
                }
                LoadFrames(selectedGifFrames);
                string message = string.Format("Loaded {0} frames out of {1} frames of the gif, selecting every {2}th frame.", selectedGifFrames.Count, imagesList.Count, gifToFramesEvery);
                this.AddToLog(message);
            }
        }

        public void LoadMp4Frames(List<string> imagesList)
        {
            if (imagesList != null && imagesList.Count > 0)
            {
                LoadFrames(imagesList);
                string message = string.Format("Loaded {0} frames from the movie.", imagesList.Count);
                this.AddToLog(message);
            }
        }

        public void LoadFrames(List<string> imagesList)
        {
            if (imagesList != null && imagesList.Count > 0)
            {
                int numbering = 1;
                //int x = 10; int y = 10; int maxHeight = -1;
                for (int i = 0; i < imagesList.Count; i++)
                {
                    string imgFile = imagesList[i];
                    string imgName = Path.GetFileName(imgFile);
                    ImageDetails imgDet = new ImageDetails(imgFile, imgName, numbering.ToString(), this);                  
                    this.flowLayoutPanelImages.Controls.Add(imgDet);
                    numbering++;
                }
            }
        }

        public void ProcessGifImage(string imagePath)
        {
            ShowLoadingImage();
            List<string> imgFiles = null;
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += (s, e) => {                
                imgFiles = GifUtil.SplitGifToFRames(imagePath);
                ImagesList = imgFiles;
            };
            bw.RunWorkerCompleted += (s, e) => {
                LoadGifImages(this.ImagesList);
                DoAfterProcessOperations();
            };

            bw.RunWorkerAsync();
        }

        private void ShowLoadingImage()
        {
            flowLayoutPanelImages.SendToBack();
            pictureBoxLoading.Visible = true;
        }

        public void ProcessMp4Movie(string mediaPath)
        {
            ShowLoadingImage();
            List<string> imgFiles = null;
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += (s, e) => {
                VideoUtil vu = new VideoUtil(this);
                imgFiles = vu.TakeKeyFrames(mediaPath);
                ImagesList = imgFiles;
            };
            bw.RunWorkerCompleted += (s, e) =>
            {
                LoadMp4Frames(this.ImagesList);
                DoAfterProcessOperations();
            };

            bw.RunWorkerAsync();
        }

        private void DoAfterProcessOperations()
        {
            HideLoadingImage();
            actionsToolStripMenuItem.Enabled = true;
        }

        public void ProcessImage(string mediaPath)
        {
            throw new NotImplementedException();
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            // When window state changes
            if (WindowState != LastWindowState)
            {
                LastWindowState = WindowState;


                if (WindowState == FormWindowState.Maximized)
                {
                    // Maximized!
                    //MessageBox.Show("Maximized");
                    splitContainer1.SplitterDistance = pictureBoxImage.Width + 20;
                    //MessageBox.Show(splitContainer1.Panel2.Width.ToString())
                    //MessageBox.Show("Flow " + flowLayoutPanelImages.Width.ToString());
                    flowLayoutPanelImages.MaximumSize = new System.Drawing.Size(splitContainer1.Panel2.Width - panelRight.Width - flowLayoutPanelImages.Padding.Horizontal, splitContainer1.Panel2.Height);
                    //MessageBox.Show("splitContainer1 Panel2 " + splitContainer1.Panel2.Width.ToString());
                    //MessageBox.Show("Flow after" + flowLayoutPanelImages.Width.ToString());
                    //panel1.Visible = false;
                    //MessageBox.Show("splitContainer1 " + splitContainer1.Width.ToString());
                    
                }
                if (WindowState == FormWindowState.Normal)
                {
                    //Restored
                    //MessageBox.Show("Restored");
                    splitContainer1.SplitterDistance = pictureBoxImage.Width + 20;
                    //MessageBox.Show("Flow " + flowLayoutPanelImages.Width.ToString());
                    //MessageBox.Show("splitContainer1 " + splitContainer1.Width.ToString());
                    flowLayoutPanelImages.MaximumSize = new System.Drawing.Size(splitContainer1.Panel2.Width - panelRight.Width - flowLayoutPanelImages.Padding.Horizontal, splitContainer1.Panel2.Height);

                    //MessageBox.Show(splitContainer1.Panel2.Width.ToString());

                }
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void clearAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClearAll();
        }

        private void describeAllImagesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < flowLayoutPanelImages.Controls.Count; i++)
            {
                if (flowLayoutPanelImages.Controls[i] is ImageDetails)
                {
                    //await Task.Run(() => ();
                    ((ImageDetails)flowLayoutPanelImages.Controls[i]).DescribeImage(sender, e);
                    //Thread.Sleep(2000);

                }
            }
        }

        private void calculateJointProbabilityForAllDescriptionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < flowLayoutPanelImages.Controls.Count; i++)
            {
                if (flowLayoutPanelImages.Controls[i] is ImageDetails)
                {
                    //await Task.Run(() => ();
                    ((ImageDetails)flowLayoutPanelImages.Controls[i]).CalculateJointProbability(sender, e);
                    //Thread.Sleep(2000);

                }
            }
        }

        private void preferencesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormPreferences fp = new FormPreferences();
            fp.ShowDialog();
        }

        private void openTempFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string tempFolder = Path.Combine(Util.GetExeApplicationPath() , "Temp");
                if (Directory.Exists(tempFolder))
                    Process.Start(tempFolder);
                else
                    MessageBox.Show("Temp folder is empty", "Information", MessageBoxButtons.OK,MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                string errorMessage = "Error while trying to open Temp folder";
                MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Logger.log.Error(errorMessage + Environment.NewLine + ex.Message);
            }
        }

        private void openLogFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string logFilePath = Constants.LogFilePath;
                if (File.Exists(logFilePath))
                    Process.Start("notepad.exe", logFilePath);
                else
                    MessageBox.Show("Log file is empty", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                string errorMessage = "Error while trying to open log file";
                MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Logger.log.Error(errorMessage + Environment.NewLine + ex.Message);
            }
        }

        private void clearTemporaryFilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show("Are you sure you want to delete all game history: logs, graph histories, etc.", "Clear History", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    Util.DeleteFolderIfExists(Path.Combine(Util.GetExeApplicationPath(), "Temp"));                    
                    Util.DeleteFileIfExists(Constants.LogFilePath);
                    MessageBox.Show("Application history deleted successfully.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                string errorMessage = "Error while trying to clear history - see log.";
                Logger.log.Error(errorMessage + Environment.NewLine + ex.Message);
                MessageBox.Show(errorMessage, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void SetPanelRightTagsContent(string imageNumbering, List<string> tagsList)
        {
            panelRight.Controls.Clear();
            Font fontHeader = new Font(this.Font, FontStyle.Bold);
            panelRight.Controls.Add(new Label{ Text = "Images " + imageNumbering +" Tags",Font = fontHeader, AutoSize =true, Padding = new Padding(0,0,0,10) });
            panelRight.Controls.Add(AddSeperator());
            int numbering = 1;
            string tagString = "";
            foreach (string tag in tagsList)
            {
                tagString = numbering.ToString() + ". " + tag;
                numbering++;
                panelRight.Controls.Add(new Label { Text = tagString });
            }



        }
       
        public Label AddSeperator()
        {
            Label labelSep = new Label();
            labelSep.AutoSize = false;
            labelSep.Height = 2;
            labelSep.Padding =  new Padding(0,0,0,10);
            labelSep.BorderStyle = BorderStyle.Fixed3D;
            return labelSep;
        }
       
    }
}
