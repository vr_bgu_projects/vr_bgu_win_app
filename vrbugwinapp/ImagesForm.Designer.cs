﻿namespace vrbugwinapp
{
    partial class ImagesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImagesForm));
            this.panelImages = new System.Windows.Forms.Panel();
            this.pictureBoxLoading = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.actionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.describeAllImagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculateJointProbabilityForAllDescriptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panelImages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLoading)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelImages
            // 
            this.panelImages.AutoScroll = true;
            this.panelImages.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panelImages.Controls.Add(this.pictureBoxLoading);
            this.panelImages.Controls.Add(this.menuStrip1);
            this.panelImages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelImages.Location = new System.Drawing.Point(0, 0);
            this.panelImages.Name = "panelImages";
            this.panelImages.Size = new System.Drawing.Size(969, 428);
            this.panelImages.TabIndex = 0;
            // 
            // pictureBoxLoading
            // 
            this.pictureBoxLoading.ErrorImage = global::vrbugwinapp.Properties.Resources.hex_loader2;
            this.pictureBoxLoading.Image = global::vrbugwinapp.Properties.Resources.hex_loader2;
            this.pictureBoxLoading.InitialImage = global::vrbugwinapp.Properties.Resources.hex_loader2;
            this.pictureBoxLoading.Location = new System.Drawing.Point(287, 90);
            this.pictureBoxLoading.Name = "pictureBoxLoading";
            this.pictureBoxLoading.Size = new System.Drawing.Size(392, 275);
            this.pictureBoxLoading.TabIndex = 0;
            this.pictureBoxLoading.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.actionsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(969, 28);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // actionsToolStripMenuItem
            // 
            this.actionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.describeAllImagesToolStripMenuItem,
            this.calculateJointProbabilityForAllDescriptionsToolStripMenuItem});
            this.actionsToolStripMenuItem.Name = "actionsToolStripMenuItem";
            this.actionsToolStripMenuItem.Size = new System.Drawing.Size(70, 24);
            this.actionsToolStripMenuItem.Text = "Actions";
            // 
            // describeAllImagesToolStripMenuItem
            // 
            this.describeAllImagesToolStripMenuItem.Name = "describeAllImagesToolStripMenuItem";
            this.describeAllImagesToolStripMenuItem.Size = new System.Drawing.Size(389, 26);
            this.describeAllImagesToolStripMenuItem.Text = "Describe All Images";
            this.describeAllImagesToolStripMenuItem.Click += new System.EventHandler(this.describeAllImagesToolStripMenuItem_Click);
            // 
            // calculateJointProbabilityForAllDescriptionsToolStripMenuItem
            // 
            this.calculateJointProbabilityForAllDescriptionsToolStripMenuItem.Name = "calculateJointProbabilityForAllDescriptionsToolStripMenuItem";
            this.calculateJointProbabilityForAllDescriptionsToolStripMenuItem.Size = new System.Drawing.Size(389, 26);
            this.calculateJointProbabilityForAllDescriptionsToolStripMenuItem.Text = "Calculate Joint Probability For All Descriptions";
            this.calculateJointProbabilityForAllDescriptionsToolStripMenuItem.Click += new System.EventHandler(this.calculateJointProbabilityForAllDescriptionsToolStripMenuItem_Click);
            // 
            // ImagesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(969, 428);
            this.Controls.Add(this.panelImages);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ImagesForm";
            this.Text = "Media Analysis";
            this.panelImages.ResumeLayout(false);
            this.panelImages.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLoading)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelImages;
        private System.Windows.Forms.PictureBox pictureBoxLoading;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem actionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem describeAllImagesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculateJointProbabilityForAllDescriptionsToolStripMenuItem;
    }
}