﻿namespace vrbugwinapp
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadMediaUrlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadExamplesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gifExampleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mp4ExampleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.actionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.describeAllImagesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculateJointProbabilityForAllDescriptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aPIKeysToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openLogFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openTempFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearTemporaryFilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.buttonAnalyzeFrame = new System.Windows.Forms.Button();
            this.textBoxMediaPath = new System.Windows.Forms.TextBox();
            this.toolTipImage = new System.Windows.Forms.ToolTip(this.components);
            this.pictureBoxImage = new System.Windows.Forms.PictureBox();
            this.panelLog = new System.Windows.Forms.Panel();
            this.listBoxLog = new System.Windows.Forms.ListBox();
            this.axWindowsMediaPlayer1 = new AxWMPLib.AxWindowsMediaPlayer();
            this.panelLeftSideMenu = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.flowLayoutPanelImages = new System.Windows.Forms.FlowLayoutPanel();
            this.pictureBoxLoading = new System.Windows.Forms.PictureBox();
            this.panelRight = new System.Windows.Forms.FlowLayoutPanel();
            this.labeMediaInfoHeader = new System.Windows.Forms.Label();
            this.panelMediaInfo = new System.Windows.Forms.Panel();
            this.labelMediaInfo = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).BeginInit();
            this.panelLog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer1)).BeginInit();
            this.panelLeftSideMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.flowLayoutPanelImages.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLoading)).BeginInit();
            this.panelMediaInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.actionsToolStripMenuItem,
            this.settingsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1068, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openImageToolStripMenuItem,
            this.loadMediaUrlToolStripMenuItem,
            this.loadExamplesToolStripMenuItem,
            this.clearAllToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openImageToolStripMenuItem
            // 
            this.openImageToolStripMenuItem.Name = "openImageToolStripMenuItem";
            this.openImageToolStripMenuItem.Size = new System.Drawing.Size(186, 26);
            this.openImageToolStripMenuItem.Text = "Open Media";
            this.openImageToolStripMenuItem.Click += new System.EventHandler(this.openImageToolStripMenuItem_Click);
            // 
            // loadMediaUrlToolStripMenuItem
            // 
            this.loadMediaUrlToolStripMenuItem.Name = "loadMediaUrlToolStripMenuItem";
            this.loadMediaUrlToolStripMenuItem.Size = new System.Drawing.Size(186, 26);
            this.loadMediaUrlToolStripMenuItem.Text = "Load Media Url";
            this.loadMediaUrlToolStripMenuItem.Click += new System.EventHandler(this.loadMediaUrlToolStripMenuItem_Click);
            // 
            // loadExamplesToolStripMenuItem
            // 
            this.loadExamplesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gifExampleToolStripMenuItem,
            this.mp4ExampleToolStripMenuItem});
            this.loadExamplesToolStripMenuItem.Name = "loadExamplesToolStripMenuItem";
            this.loadExamplesToolStripMenuItem.Size = new System.Drawing.Size(186, 26);
            this.loadExamplesToolStripMenuItem.Text = "Load Examples";
            // 
            // gifExampleToolStripMenuItem
            // 
            this.gifExampleToolStripMenuItem.Name = "gifExampleToolStripMenuItem";
            this.gifExampleToolStripMenuItem.Size = new System.Drawing.Size(175, 26);
            this.gifExampleToolStripMenuItem.Text = "Gif Example";
            this.gifExampleToolStripMenuItem.Click += new System.EventHandler(this.gifExampleToolStripMenuItem_Click);
            // 
            // mp4ExampleToolStripMenuItem
            // 
            this.mp4ExampleToolStripMenuItem.Name = "mp4ExampleToolStripMenuItem";
            this.mp4ExampleToolStripMenuItem.Size = new System.Drawing.Size(175, 26);
            this.mp4ExampleToolStripMenuItem.Text = "Mp4 Example";
            this.mp4ExampleToolStripMenuItem.Click += new System.EventHandler(this.mp4ExampleToolStripMenuItem_Click);
            // 
            // clearAllToolStripMenuItem
            // 
            this.clearAllToolStripMenuItem.Name = "clearAllToolStripMenuItem";
            this.clearAllToolStripMenuItem.Size = new System.Drawing.Size(186, 26);
            this.clearAllToolStripMenuItem.Text = "Clear All";
            this.clearAllToolStripMenuItem.Click += new System.EventHandler(this.clearAllToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(186, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // actionsToolStripMenuItem
            // 
            this.actionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.describeAllImagesToolStripMenuItem,
            this.calculateJointProbabilityForAllDescriptionsToolStripMenuItem});
            this.actionsToolStripMenuItem.Enabled = false;
            this.actionsToolStripMenuItem.Name = "actionsToolStripMenuItem";
            this.actionsToolStripMenuItem.Size = new System.Drawing.Size(70, 24);
            this.actionsToolStripMenuItem.Text = "Actions";
            // 
            // describeAllImagesToolStripMenuItem
            // 
            this.describeAllImagesToolStripMenuItem.Name = "describeAllImagesToolStripMenuItem";
            this.describeAllImagesToolStripMenuItem.Size = new System.Drawing.Size(389, 26);
            this.describeAllImagesToolStripMenuItem.Text = "Describe All Images";
            this.describeAllImagesToolStripMenuItem.Click += new System.EventHandler(this.describeAllImagesToolStripMenuItem_Click);
            // 
            // calculateJointProbabilityForAllDescriptionsToolStripMenuItem
            // 
            this.calculateJointProbabilityForAllDescriptionsToolStripMenuItem.Name = "calculateJointProbabilityForAllDescriptionsToolStripMenuItem";
            this.calculateJointProbabilityForAllDescriptionsToolStripMenuItem.Size = new System.Drawing.Size(389, 26);
            this.calculateJointProbabilityForAllDescriptionsToolStripMenuItem.Text = "Calculate Joint Probability For All Descriptions";
            this.calculateJointProbabilityForAllDescriptionsToolStripMenuItem.Click += new System.EventHandler(this.calculateJointProbabilityForAllDescriptionsToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aPIKeysToolStripMenuItem,
            this.openLogFileToolStripMenuItem,
            this.openTempFolderToolStripMenuItem,
            this.clearTemporaryFilesToolStripMenuItem,
            this.preferencesToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(74, 24);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // aPIKeysToolStripMenuItem
            // 
            this.aPIKeysToolStripMenuItem.Name = "aPIKeysToolStripMenuItem";
            this.aPIKeysToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.aPIKeysToolStripMenuItem.Text = "API Keys";
            this.aPIKeysToolStripMenuItem.Click += new System.EventHandler(this.aPIKeysToolStripMenuItem_Click);
            // 
            // openLogFileToolStripMenuItem
            // 
            this.openLogFileToolStripMenuItem.Name = "openLogFileToolStripMenuItem";
            this.openLogFileToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.openLogFileToolStripMenuItem.Text = "Open Log File";
            this.openLogFileToolStripMenuItem.Click += new System.EventHandler(this.openLogFileToolStripMenuItem_Click);
            // 
            // openTempFolderToolStripMenuItem
            // 
            this.openTempFolderToolStripMenuItem.Name = "openTempFolderToolStripMenuItem";
            this.openTempFolderToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.openTempFolderToolStripMenuItem.Text = "Open Temp Folder";
            this.openTempFolderToolStripMenuItem.Click += new System.EventHandler(this.openTempFolderToolStripMenuItem_Click);
            // 
            // clearTemporaryFilesToolStripMenuItem
            // 
            this.clearTemporaryFilesToolStripMenuItem.Name = "clearTemporaryFilesToolStripMenuItem";
            this.clearTemporaryFilesToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.clearTemporaryFilesToolStripMenuItem.Text = "Clear History";
            this.clearTemporaryFilesToolStripMenuItem.Click += new System.EventHandler(this.clearTemporaryFilesToolStripMenuItem_Click);
            // 
            // preferencesToolStripMenuItem
            // 
            this.preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
            this.preferencesToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.preferencesToolStripMenuItem.Text = "Preferences...";
            this.preferencesToolStripMenuItem.Click += new System.EventHandler(this.preferencesToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // buttonAnalyzeFrame
            // 
            this.buttonAnalyzeFrame.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(57)))), ((int)(((byte)(85)))));
            this.buttonAnalyzeFrame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAnalyzeFrame.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAnalyzeFrame.ForeColor = System.Drawing.Color.White;
            this.buttonAnalyzeFrame.Location = new System.Drawing.Point(-16, 13);
            this.buttonAnalyzeFrame.Name = "buttonAnalyzeFrame";
            this.buttonAnalyzeFrame.Size = new System.Drawing.Size(233, 40);
            this.buttonAnalyzeFrame.TabIndex = 2;
            this.buttonAnalyzeFrame.Text = "Describe Media";
            this.buttonAnalyzeFrame.UseVisualStyleBackColor = false;
            this.buttonAnalyzeFrame.Click += new System.EventHandler(this.buttonDescribeMedia_Click);
            // 
            // textBoxMediaPath
            // 
            this.textBoxMediaPath.BackColor = System.Drawing.Color.White;
            this.textBoxMediaPath.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxMediaPath.Location = new System.Drawing.Point(5, 3);
            this.textBoxMediaPath.Multiline = true;
            this.textBoxMediaPath.Name = "textBoxMediaPath";
            this.textBoxMediaPath.ReadOnly = true;
            this.textBoxMediaPath.Size = new System.Drawing.Size(289, 56);
            this.textBoxMediaPath.TabIndex = 3;
            this.textBoxMediaPath.Visible = false;
            this.textBoxMediaPath.Enter += new System.EventHandler(this.textBoxMediaPath_Enter);
            this.textBoxMediaPath.GotFocus += new System.EventHandler(this.textBoxMediaPath_GotFocus);
            this.textBoxMediaPath.Leave += new System.EventHandler(this.textBoxMediaPath_Leave);
            this.textBoxMediaPath.MouseUp += new System.Windows.Forms.MouseEventHandler(this.textBoxMediaPath_MouseUp);
            // 
            // pictureBoxImage
            // 
            this.pictureBoxImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxImage.ErrorImage = global::vrbugwinapp.Properties.Resources.multimedia_icon;
            this.pictureBoxImage.Image = global::vrbugwinapp.Properties.Resources.multimedia_icon;
            this.pictureBoxImage.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxImage.InitialImage")));
            this.pictureBoxImage.Location = new System.Drawing.Point(8, 13);
            this.pictureBoxImage.Name = "pictureBoxImage";
            this.pictureBoxImage.Size = new System.Drawing.Size(318, 301);
            this.pictureBoxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxImage.TabIndex = 1;
            this.pictureBoxImage.TabStop = false;
            this.toolTipImage.SetToolTip(this.pictureBoxImage, "Click here to open image");
            this.pictureBoxImage.Click += new System.EventHandler(this.pictureBoxImage_Click);
            // 
            // panelLog
            // 
            this.panelLog.Controls.Add(this.listBoxLog);
            this.panelLog.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelLog.Location = new System.Drawing.Point(0, 556);
            this.panelLog.Name = "panelLog";
            this.panelLog.Size = new System.Drawing.Size(1068, 162);
            this.panelLog.TabIndex = 4;
            // 
            // listBoxLog
            // 
            this.listBoxLog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(234)))), ((int)(((byte)(234)))));
            this.listBoxLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxLog.FormattingEnabled = true;
            this.listBoxLog.ItemHeight = 16;
            this.listBoxLog.Location = new System.Drawing.Point(0, 0);
            this.listBoxLog.Margin = new System.Windows.Forms.Padding(10);
            this.listBoxLog.Name = "listBoxLog";
            this.listBoxLog.ScrollAlwaysVisible = true;
            this.listBoxLog.Size = new System.Drawing.Size(1068, 162);
            this.listBoxLog.TabIndex = 6;
            // 
            // axWindowsMediaPlayer1
            // 
            this.axWindowsMediaPlayer1.Enabled = true;
            this.axWindowsMediaPlayer1.Location = new System.Drawing.Point(16, 19);
            this.axWindowsMediaPlayer1.Name = "axWindowsMediaPlayer1";
            this.axWindowsMediaPlayer1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axWindowsMediaPlayer1.OcxState")));
            this.axWindowsMediaPlayer1.Size = new System.Drawing.Size(303, 291);
            this.axWindowsMediaPlayer1.TabIndex = 5;
            // 
            // panelLeftSideMenu
            // 
            this.panelLeftSideMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(57)))), ((int)(((byte)(85)))));
            this.panelLeftSideMenu.Controls.Add(this.buttonAnalyzeFrame);
            this.panelLeftSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeftSideMenu.Location = new System.Drawing.Point(0, 28);
            this.panelLeftSideMenu.Margin = new System.Windows.Forms.Padding(10);
            this.panelLeftSideMenu.Name = "panelLeftSideMenu";
            this.panelLeftSideMenu.Padding = new System.Windows.Forms.Padding(10);
            this.panelLeftSideMenu.Size = new System.Drawing.Size(200, 528);
            this.panelLeftSideMenu.TabIndex = 6;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(200, 28);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panelMediaInfo);
            this.splitContainer1.Panel1.Controls.Add(this.pictureBoxImage);
            this.splitContainer1.Panel1.Controls.Add(this.axWindowsMediaPlayer1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Panel2.Controls.Add(this.flowLayoutPanelImages);
            this.splitContainer1.Size = new System.Drawing.Size(868, 528);
            this.splitContainer1.SplitterDistance = 352;
            this.splitContainer1.TabIndex = 7;
            // 
            // flowLayoutPanelImages
            // 
            this.flowLayoutPanelImages.AutoScroll = true;
            this.flowLayoutPanelImages.AutoSize = true;
            this.flowLayoutPanelImages.Controls.Add(this.pictureBoxLoading);
            this.flowLayoutPanelImages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelImages.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanelImages.Name = "flowLayoutPanelImages";
            this.flowLayoutPanelImages.Padding = new System.Windows.Forms.Padding(30);
            this.flowLayoutPanelImages.Size = new System.Drawing.Size(512, 528);
            this.flowLayoutPanelImages.TabIndex = 3;
            // 
            // pictureBoxLoading
            // 
            this.pictureBoxLoading.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBoxLoading.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBoxLoading.ErrorImage = global::vrbugwinapp.Properties.Resources.hex_loader2;
            this.pictureBoxLoading.Image = global::vrbugwinapp.Properties.Resources.hex_loader2;
            this.pictureBoxLoading.InitialImage = global::vrbugwinapp.Properties.Resources.hex_loader2;
            this.pictureBoxLoading.Location = new System.Drawing.Point(33, 33);
            this.pictureBoxLoading.Name = "pictureBoxLoading";
            this.pictureBoxLoading.Size = new System.Drawing.Size(263, 210);
            this.pictureBoxLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxLoading.TabIndex = 2;
            this.pictureBoxLoading.TabStop = false;
            // 
            // panelRight
            // 
            this.panelRight.BackColor = System.Drawing.Color.Transparent;
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(892, 28);
            this.panelRight.Name = "panelRight";
            this.panelRight.Padding = new System.Windows.Forms.Padding(20);
            this.panelRight.Size = new System.Drawing.Size(176, 528);
            this.panelRight.TabIndex = 0;
            this.panelRight.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            // 
            // labeMediaInfoHeader
            // 
            this.labeMediaInfoHeader.AutoSize = true;
            this.labeMediaInfoHeader.ForeColor = System.Drawing.Color.RoyalBlue;
            this.labeMediaInfoHeader.Location = new System.Drawing.Point(5, 64);
            this.labeMediaInfoHeader.Name = "labeMediaInfoHeader";
            this.labeMediaInfoHeader.Size = new System.Drawing.Size(124, 17);
            this.labeMediaInfoHeader.TabIndex = 0;
            this.labeMediaInfoHeader.Text = "Media Information:";
            // 
            // panelMediaInfo
            // 
            this.panelMediaInfo.AutoScroll = true;
            this.panelMediaInfo.Controls.Add(this.labelMediaInfo);
            this.panelMediaInfo.Controls.Add(this.textBoxMediaPath);
            this.panelMediaInfo.Controls.Add(this.labeMediaInfoHeader);
            this.panelMediaInfo.Location = new System.Drawing.Point(8, 320);
            this.panelMediaInfo.Name = "panelMediaInfo";
            this.panelMediaInfo.Size = new System.Drawing.Size(318, 281);
            this.panelMediaInfo.TabIndex = 6;
            this.panelMediaInfo.Visible = false;
            // 
            // labelMediaInfo
            // 
            this.labelMediaInfo.AutoSize = true;
            this.labelMediaInfo.ForeColor = System.Drawing.Color.Black;
            this.labelMediaInfo.Location = new System.Drawing.Point(4, 84);
            this.labelMediaInfo.Name = "labelMediaInfo";
            this.labelMediaInfo.Size = new System.Drawing.Size(32, 17);
            this.labelMediaInfo.TabIndex = 1;
            this.labelMediaInfo.Text = "###";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1068, 718);
            this.Controls.Add(this.panelRight);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panelLeftSideMenu);
            this.Controls.Add(this.panelLog);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "BGU Media Analyzer";
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).EndInit();
            this.panelLog.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayer1)).EndInit();
            this.panelLeftSideMenu.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.flowLayoutPanelImages.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLoading)).EndInit();
            this.panelMediaInfo.ResumeLayout(false);
            this.panelMediaInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openImageToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button buttonAnalyzeFrame;
        private System.Windows.Forms.TextBox textBoxMediaPath;
        private System.Windows.Forms.ToolTip toolTipImage;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aPIKeysToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadMediaUrlToolStripMenuItem;
        private System.Windows.Forms.Panel panelLog;
        public  System.Windows.Forms.ListBox listBoxLog;
        private System.Windows.Forms.ToolStripMenuItem loadExamplesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gifExampleToolStripMenuItem;
        private AxWMPLib.AxWindowsMediaPlayer axWindowsMediaPlayer1;
        private System.Windows.Forms.ToolStripMenuItem mp4ExampleToolStripMenuItem;
        private System.Windows.Forms.Panel panelLeftSideMenu;
        private System.Windows.Forms.ToolStripMenuItem actionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem describeAllImagesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem calculateJointProbabilityForAllDescriptionsToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBoxImage;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.PictureBox pictureBoxLoading;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearAllToolStripMenuItem;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelImages;
        private System.Windows.Forms.ToolStripMenuItem openLogFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openTempFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearTemporaryFilesToolStripMenuItem;
        private System.Windows.Forms.FlowLayoutPanel panelRight;
        private System.Windows.Forms.Panel panelMediaInfo;
        private System.Windows.Forms.Label labelMediaInfo;
        private System.Windows.Forms.Label labeMediaInfoHeader;
    }
}

