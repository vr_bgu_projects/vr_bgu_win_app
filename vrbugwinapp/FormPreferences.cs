﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using vrbugwinapp.Helpers;

namespace vrbugwinapp
{
    public partial class FormPreferences : Form
    {
        public FormPreferences()
        {
            InitializeComponent();
            LoadConfigFileToTextBox();
            LoadConfigFileToTreeView();
            ShowViewMode();
        }

        private void LoadConfigFileToTreeView()
        {
            treeXml.Nodes.Clear();
            // Load the XML Document
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.LoadXml(Config.GetConfigFileContent());
                //doc.Load("");
            }
            catch (Exception err)
            {

                MessageBox.Show(err.Message);
                return;
            }

            ConvertXmlNodeToTreeNode(doc, treeXml.Nodes);
            treeXml.Nodes[0].ExpandAll();
            treeXml.LabelEdit = true;

        }


        private void ConvertXmlNodeToTreeNode(XmlNode xmlNode, TreeNodeCollection treeNodes)
        {
            TreeNode newTreeNode = treeNodes.Add(xmlNode.Name);
            switch (xmlNode.NodeType)
            {
                case XmlNodeType.ProcessingInstruction:
                case XmlNodeType.XmlDeclaration:
                    newTreeNode.Text = "<?" + xmlNode.Name + " " +
                      xmlNode.Value + "?>";
                    break;
                case XmlNodeType.Element:
                    newTreeNode.Text = "<" + xmlNode.Name + ">";
                    break;
                case XmlNodeType.Attribute:
                    newTreeNode.Text = "ATTRIBUTE: " + xmlNode.Name;
                    break;
                case XmlNodeType.Text:
                case XmlNodeType.CDATA:
                    newTreeNode.Text = xmlNode.Value;
                    break;
                case XmlNodeType.Comment:
                    newTreeNode.Text = "<!--" + xmlNode.Value + "-->";
                    break;
            }
            if (xmlNode.Attributes != null)
            {
                foreach (XmlAttribute attribute in xmlNode.Attributes)
                {
                    ConvertXmlNodeToTreeNode(attribute, newTreeNode.Nodes);
                }
            }
            foreach (XmlNode childNode in xmlNode.ChildNodes)
            {
                ConvertXmlNodeToTreeNode(childNode, newTreeNode.Nodes);
            }
        }

        private void LoadConfigFileToTextBox()
        {
            try
            {
                //XmlReader xmlFile;
                //Uri uri = new Uri(Config.GetConfigFilePath());
                //XmlReader xmlReader = XmlReader.Create(new StringReader(Config.GetConfigFileContent()));

                //DataSet dataSet = new DataSet();
                //DataTable dataTable = new DataTable("table1");
                //dataTable.Columns.Add("configuration", typeof(string));
                //dataSet.Tables.Add(dataTable);

                //string xmlData = Config.GetConfigFileContent();

                //System.IO.StringReader xmlSR = new System.IO.StringReader(xmlData);

                //dataSet.ReadXml(xmlSR, XmlReadMode.IgnoreSchema);

                //xmlFile = XmlReader.Create(uri.AbsoluteUri, new XmlReaderSettings());
                //DataSet ds = new DataSet();
                //ds.ReadXml(xmlFile);
                //dataGridViewSettings.DataSource = dataSet.Tables[0];
                textBoxSettings.Text = Config.GetConfigFileContent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {
                string configFilePath = Config.GetConfigFilePath();
                if (!Util.IsValidXml(textBoxSettings.Text))
                {
                    MessageBox.Show("XML Confiuration is not valid, check it.");
                    return;
                }
                File.Copy(configFilePath, configFilePath + Util.RemoveAllNonAlphanumericCharacters(DateTime.Now.ToString(), "_") + ".bak");
                File.WriteAllText(configFilePath, textBoxSettings.Text);
                this.Close();

            }
            catch (Exception ex)
            {
                string errorMesage = ex.ToString();
                MessageBox.Show(errorMesage);
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void treeXml_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode selectedNode = e.Node;
            if (selectedNode != null)
            {
                if (selectedNode.Parent == null)
                {
                    // Root-level node  
                }
                else
                {
                    // Child node
                    //MessageBox.Show(e.Node.Text);
                    //EditThisNode(e.Node.Text);
                }
            }
            else
            {
                // A node hasn't been selected.
            }
        }

        private void EditThisNode(string text)
        {
            // This test that the InputBox can handle more newline than one.
            InputBoxResult nodeTextEdit = InputBox.Show("Edit the value"
                          , "Edit the value", "", 300, 0);

            if (nodeTextEdit.ReturnCode == DialogResult.OK)
            // MessageBox.Show(mediaUrlBox.Text);
            {
                try
                {
                    //nodeTextEdit.Text
                }
                catch (Exception ex)
                {

                    throw;
                }
                //pictureBoxImage.ImageLocation = (mediaUrlBox.Text);
            }
        }

        private void buttonEditMode_Click(object sender, EventArgs e)
        {
            if (buttonEditMode.Text.Contains("Edit"))
                ShowEditMode();
            else if (buttonEditMode.Text.Contains("View"))
                ShowViewMode();
        }

        private void ShowEditMode()
        {
            buttonCancel.Enabled = true;
            buttonSave.Enabled = true;
            treeXml.Visible = false;
            textBoxSettings.Visible = true;
            buttonEditMode.Text = "View Mode";
        }

        private void ShowViewMode()
        {
            buttonCancel.Enabled = false;
            buttonSave.Enabled = false;
            treeXml.Visible = true;
            textBoxSettings.Visible = false;
            buttonEditMode.Text = "Edit Mode";          
        }
    }
}
